export DEVELOPER_DIR=/Applications/Xcode.app/Contents/Developer
export GYP_CROSSCOMPILE=1
export GYP_GENERATORS=ninja

function build_ios() {
    echo 'Starting ARMv7'
    export GYP_DEFINES="OS=ios target_arch=arm arm_version=7 build_with_libjingle=1 build_with_chromium=0 libjingle_objc=0 enable_protobuf=0 rtc_use_openmax_dl=0 clang_xcode=1 clang=1 include_tests=0 chromium_ios_signing=0"
    export GYP_GENERATOR_FLAGS="output_dir=out_ios"
    pushd src
    gclient runhooks
    ninja -C out_ios/Release-iphoneos
    popd
}

function build_ios64() {
    echo 'Starting ARM64'
    export GYP_DEFINES="OS=ios target_arch=arm64 target_subarch=arm64 build_with_libjingle=1 build_with_chromium=0 libjingle_objc=0 enable_protobuf=0 rtc_use_openmax_dl=0 clang_xcode=1 clang=1 include_tests=0 chromium_ios_signing=0"
    export GYP_GENERATOR_FLAGS="output_dir=out_ios64"
    pushd src
    gclient runhooks
    ninja -C out_ios64/Release-iphoneos
    popd
}

function build_sim() {
    echo 'Starting i386'
    export GYP_DEFINES="OS=ios target_arch=ia32 build_with_libjingle=1 build_with_chromium=0 libjingle_objc=0 enable_protobuf=0 rtc_use_openmax_dl=0 clang_xcode=1 clang=1 include_tests=0 chromium_ios_signing=0"
    export GYP_GENERATOR_FLAGS="output_dir=out_sim"
    pushd src
    gclient runhooks
    ninja -C out_sim/Release-iphonesimulator
    popd
}

function build_sim64() {
    echo 'Starting x86-64'
    export GYP_DEFINES="OS=ios target_arch=x64 build_with_libjingle=1 build_with_chromium=0 libjingle_objc=0 enable_protobuf=0 rtc_use_openmax_dl=0 clang_xcode=1 clang=1 include_tests=0 chromium_ios_signing=0"
    export GYP_GENERATOR_FLAGS="output_dir=out_sim64"
    pushd src
    gclient runhooks
    ninja -C out_sim64/Release-iphonesimulator
    popd
}

function build_ios_debug() {
    echo 'Starting ARMv7 debug'
    export GYP_DEFINES="OS=ios target_arch=arm arm_version=7 build_with_libjingle=1 build_with_chromium=0 libjingle_objc=0 enable_protobuf=0 rtc_use_openmax_dl=0 clang_xcode=1 clang=1 include_tests=0 chromium_ios_signing=0"
    export GYP_GENERATOR_FLAGS="output_dir=out_ios"
    pushd src
    gclient runhooks
    ninja -C out_ios/Debug-iphoneos
    popd
}

function build_ios64_debug() {
    echo 'Starting ARM64 debug'
    export GYP_DEFINES="OS=ios target_arch=arm64 target_subarch=arm64 build_with_libjingle=1 build_with_chromium=0 libjingle_objc=0 enable_protobuf=0 rtc_use_openmax_dl=0 clang_xcode=1 clang=1 include_tests=0 chromium_ios_signing=0"
    export GYP_GENERATOR_FLAGS="output_dir=out_ios64"
    pushd src
    gclient runhooks
    ninja -C out_ios64/Debug-iphoneos
    popd
}

function build_sim_debug() {
    echo 'Starting i386 debug'
    export GYP_DEFINES="OS=ios target_arch=ia32 build_with_libjingle=1 build_with_chromium=0 libjingle_objc=0 enable_protobuf=0 rtc_use_openmax_dl=0 clang_xcode=1 clang=1 include_tests=0 chromium_ios_signing=0"
    export GYP_GENERATOR_FLAGS="output_dir=out_sim"
    pushd src
    gclient runhooks
    ninja -C out_sim/Debug-iphonesimulator
    popd
}

function build_sim64_debug() {
    echo 'Starting x86-64 debug'
    export GYP_DEFINES="OS=ios target_arch=x64 build_with_libjingle=1 build_with_chromium=0 libjingle_objc=0 enable_protobuf=0 rtc_use_openmax_dl=0 clang_xcode=1 clang=1 include_tests=0 chromium_ios_signing=0"
    export GYP_GENERATOR_FLAGS="output_dir=out_sim64"
    pushd src
    gclient runhooks
    ninja -C out_sim64/Debug-iphonesimulator
    popd
}

function build_debug() {
    build_ios_debug
    build_ios64_debug
    build_sim_debug
    build_sim64_debug
    python universal.py -debug
}

function build_release() {
    build_ios
    build_ios64
    build_sim
    build_sim64
    python universal.py
}

