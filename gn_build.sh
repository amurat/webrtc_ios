function build_ios() {
  pushd src
  gn gen out/Debug-device-ios --args='target_os="ios"                     
                                      target_cpu="arm64"                  
                                      is_component_build=false            
                                      linux_use_bundled_binutils=false    
                                      additional_target_cpus = [ "arm" ]  
                                      rtc_build_openmax_dl=false          
                                      use_xcode_clang=true                
                                      is_clang=true                       
                                      ios_enable_code_signing=false       
                                      rtc_enable_protobuf=true            
                                      rtc_use_objc_h264=true'             
  ninja -C out/Debug-device-ios
  popd
}

