#!/usr/bin/python

import os
import shutil
import sys
from distutils.file_util import copy_file

indir = 'src'
outdir = 'headers'

def headers():
  for root, dirs, files in os.walk(indir):
    for f in files:
      if f.endswith('.h'):
        in_path = os.path.join(root, f)
        out_path = os.path.join(outdir, in_path[4:]) # hack
        if not os.path.exists(os.path.dirname(out_path)):
          os.makedirs(os.path.dirname(out_path))
        copy_file(in_path, out_path)
        print "Copying " + f
        
# Copy headers
if __name__ == "__main__":
  headers()
