// Copyright 2013 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#ifndef IOS_WEB_TEST_WEB_TEST_SUITE_H_
#define IOS_WEB_TEST_WEB_TEST_SUITE_H_

#include "ios/web/public/test/web_test_suite.h"

#endif  // IOS_WEB_TEST_WEB_TEST_SUITE_H_
