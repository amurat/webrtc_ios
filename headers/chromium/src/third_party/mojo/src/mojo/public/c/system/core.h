// Copyright 2014 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

// This is a catch-all header that includes everything.
//
// Note: This header should be compilable as C.

#ifndef THIRD_PARTY_MOJO_SRC_MOJO_PUBLIC_C_SYSTEM_CORE_H_
#define THIRD_PARTY_MOJO_SRC_MOJO_PUBLIC_C_SYSTEM_CORE_H_

#include "third_party/mojo/src/mojo/public/c/system/buffer.h"
#include "third_party/mojo/src/mojo/public/c/system/data_pipe.h"
#include "third_party/mojo/src/mojo/public/c/system/functions.h"
#include "third_party/mojo/src/mojo/public/c/system/macros.h"
#include "third_party/mojo/src/mojo/public/c/system/main.h"
#include "third_party/mojo/src/mojo/public/c/system/message_pipe.h"
#include "third_party/mojo/src/mojo/public/c/system/system_export.h"
#include "third_party/mojo/src/mojo/public/c/system/types.h"

#endif  // THIRD_PARTY_MOJO_SRC_MOJO_PUBLIC_C_SYSTEM_CORE_H_
