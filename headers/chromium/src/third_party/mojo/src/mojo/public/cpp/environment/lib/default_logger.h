// Copyright 2014 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#ifndef THIRD_PARTY_MOJO_SRC_MOJO_PUBLIC_CPP_ENVIRONMENT_LIB_DEFAULT_LOGGER_H_
#define THIRD_PARTY_MOJO_SRC_MOJO_PUBLIC_CPP_ENVIRONMENT_LIB_DEFAULT_LOGGER_H_

struct MojoLogger;

namespace mojo {
namespace internal {

extern const MojoLogger kDefaultLogger;

}  // namespace internal
}  // namespace mojo

#endif  // THIRD_PARTY_MOJO_SRC_MOJO_PUBLIC_CPP_ENVIRONMENT_LIB_DEFAULT_LOGGER_H_
