export DEVELOPER_DIR=/Applications/Xcode6.4/Xcode.app/Contents/Developer
export GYP_CROSSCOMPILE=1
export GYP_DEFINES="OS=ios target_arch=x64 build_with_libjingle=1 build_with_chromium=0 libjingle_objc=0 enable_protobuf=0 rtc_use_openmax_dl=0"
export GYP_GENERATOR_FLAGS="output_dir=out_sim64"
export GYP_GENERATORS=ninja
