#!/usr/bin/python

import os
import shutil
import sys
from distutils.file_util import copy_file

if len(sys.argv) > 1:
    if sys.argv[1] == "-debug":
        debug = True
else:
    debug = False

if __name__ == "__main__":
    prune = [
        "audio_device", 
        "audio_processing",
        "audio_processing_sse2",
        "audio_processing_neon",
        "common_audio",
        "common_audio_sse2",
        "common_audio_neon",
        "common_video",
        #"directshow_baseclasses",
        "iSAC",
        "jpeg",
        "yuv",
        "rtc_base",
        "rtc_base_approved",
        "system_wrappers",
        "metrics_default",
        "video_capture_module",
        "video_capture_module_internal_impl",
        "video_processing",
        #"video_processing_sse2",
        #"video_processing_neon",
        "webrtc_common",
        "webrtc",
        "webrtc_utility",
        "rtc_sdk_common_objc"
        ]

    prune_unix = ["lib"+lib+".a" for lib in prune]
    
    if debug:
        arch_folders = ["src/out_ios/Debug-iphoneos", "src/out_ios64/Debug-iphoneos", "src/out_sim/Debug-iphonesimulator", "src/out_sim64/Debug-iphonesimulator"]
        universal_out = "out_universal_debug"
    else:
        arch_folders = ["src/out_ios/Release-iphoneos", "src/out_ios64/Release-iphoneos", "src/out_sim/Release-iphonesimulator", "src/out_sim64/Release-iphonesimulator"]
        universal_out = "out_universal"

    if not os.path.isdir(universal_out):
        os.mkdir(universal_out)
        print('Directory created at: ' + universal_out)
        
    for lib in prune_unix:
        arch_lib_paths = [os.path.join(folder,lib) for folder in arch_folders] 
        # filter out non existent libs for certain archs
        arch_lib_paths = [path for path in arch_lib_paths if os.path.exists(path)]
        if len(arch_lib_paths) > 0:
            cmd = "lipo -create " + " ".join(arch_lib_paths) + " -output " + os.path.join(universal_out, lib)
            print cmd
            os.system(cmd)
        
